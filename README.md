# Case Study Names Mapping


The goal is to map company names to associate corresponding societies or groups.

    Eg. ALTERYX = ALTERYX, INC
    ALTRAN INNOVACIÃƒÂ“N S.L. = ALTRAN INNOVACION SOCIEDAD LIMITADA
    But AMICALE DES ANCIENS DU STADE <>  AMICALE JEAN BAPTISTE SALIS

## Ideas and development

The `progress.ipynb` notebook contains development ideas and some performance metrics.

## Results

Company names are mapped to a same name if they belong to the same cluster.

Two levels of clustering are given by the script.

![alt](image/results.png)


## Installation

Create the Docker image:

`docker build -t casestudynamesmapping:latest .`

Create the Docker container:

`docker run -p 8080:8888 -v $PWD/src:/app/src casestudynamesmapping:latest &`

This command will start a jupyter server and three links to connect to it.
Copy the token given at the end of the three links and open in a browser:

http://0.0.0.0:8080/?token=\<TOKEN\>

In order to connect to the Docker server on your local machine, indicate the previous link as server URI to VSCode, and reload.

To use the script:

`docker exec -it <container name> /bin/sh`

and run in the terminal:

`python main.py <data/path_to_data.xlsx>`

or, 
`python -m cProfile -o profile.out src/main.py data/Case_study_names_mapping.xlsx`

for profiling.

___

Guillaume Bargibant