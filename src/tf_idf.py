"""Provides TF-IDF and pairwise distances.

    Uses Sklearn for both.
    Possible use of awesome_cossim_topn to speed up and keep sparse matrices,
    but hierarchical clustering doesn't seem to handle sparse matrices.

"""

import re

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_distances
# from sparse_dot_topn import awesome_cossim_topn


def get_tf_idf_distances(dataframe):
    """Returns tf-idf array and following cosine pairwise distances.

    Args:
        df (Dataframe]): must contain 'Clean name'.
    """
    vectorizer = TfidfVectorizer(analyzer=analyzer)
    tf_idf = vectorizer.fit_transform(dataframe['Clean name'])
    # distances = awesome_cossim_topn(tf_idf, tf_idf.T, 100, 0.6)
    distances = cosine_distances(tf_idf)
    return tf_idf, distances


def get_ngrams(string, n=3):
    """Generates character n-grams."""
    string = re.sub(r' ', lambda s: '', string)
    ngrams = zip(*[string[i:] for i in range(n)])
    return [''.join(ngram) for ngram in ngrams]


def analyzer(string, n=4):
    """Computes and concats all n-grams from 1 to n,
    and the split initial string.

    Args:
        string (str): the string to process.
        n (int): number of n-grams to keep
    """
    string = string.strip()
    res = []
    for i in range(1, n):
        res += get_ngrams(string, i)
    return string.split() + res
