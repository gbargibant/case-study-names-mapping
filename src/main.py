"""
    Case study names mapping.
Entry point of application:
    Usage:
        main.py <inputfile.xlsx>

This script takes a xlsx file and creates a csv file with mapped name column and scores.
"""
import sys
from clusterize import map_names


def main(argv):
    """ see clusterize.py for algorithm core. """
    inputfile = ''
    if len(argv) > 1 or not argv[0].endswith('.xlsx'):
        print('main.py <inputfile.xlsx>')
        sys.exit(2)
    inputfile = argv[0]
    print('Mapping names from {}...'.format(inputfile))
    map_names(inputfile)


if __name__ == "__main__":
    main(sys.argv[1:])
