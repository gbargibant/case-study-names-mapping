""" Cleans dataframe:
    - Non-ascii characters are normalized,
    - Special characters are deleted,
    - Stop words are deleted

    Adds column 'Clean name'.
"""

import re
from collections import Counter

from unidecode import unidecode


def clean_df(dataframe):
    """Overall cleaning: characters / stop words.

    Returns:
        dataframe (Dataframe): Cleaned dataframe, with column 'Cleaned name'.
    """
    # Normalizing non-ascii characters and deleting special characters.
    dataframe['Clean name'] = dataframe['Raw name'].apply(clean_string)
    # Retrieving, adding and deleting stop words
    stop_words = get_stop_words(dataframe)
    stop_words = add_stop_words(stop_words, 'ENGINEERING')
    stop_words = add_stop_words(stop_words, 'SERVICES')
    dataframe = del_stop_words(dataframe, stop_words)
    return dataframe


def get_stop_words(dataframe, n_stop_words=25):
    """Returns stop words list.

    Could be improved for large datasets by sampling the dataframe.
    """
    word_count = Counter()
    dataframe['Clean name'].str.split().apply(word_count.update)
    stop_words = word_count.most_common()[:n_stop_words]
    return stop_words


def del_stop_words(dataframe, stop_words):
    """Deletes all stop words. The lambda function in re.sub make
    the search continue after first match.
    """
    stop_words = r'\b|\b'.join([s for s, _ in stop_words])
    dataframe['Clean name'] = dataframe['Clean name'].apply(
        lambda s: re.sub(r'\b'+stop_words+r'\b', lambda s: '', s))
    return dataframe


def add_stop_words(stop_words, word):
    """ Adds stop word to stop_words list """
    return stop_words + [(word, 0)]


def clean_string(string):
    """ Normalizes non-ascii input and deletes other special characters. """
    string = unidecode(string)
    string = re.sub(r'[^a-zA-Z0-9 ]', lambda s: '', string)
    return string
