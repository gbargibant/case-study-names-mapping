"""Provides mapping for names, probability of being correctly mapepd, cut level clustering names."""

import hdbscan
import pandas as pd
import numpy as np

from cleaning import clean_df
from tf_idf import get_tf_idf_distances


def map_names(inputfile, cut_value=0.3):
    """ Creates outputfile with result columns.

    Args:
        cut_value (float, optional): The value from which to cut the hierarchy cluster tree.
            Defaults to 0.3.
    """
    print("Reading...")
    dataframe = pd.read_excel(inputfile)

    print("Cleaning...")
    dataframe = clean_df(dataframe)

    print("Vectorizing: TF-IDF and N-grams...")
    _, distances = get_tf_idf_distances(dataframe)

    print("Clustering...")
    clusterer = clusterize(dataframe, distances)

    print("Building mapped names...")
    dataframe = build_mapped_names(dataframe)

    print("Building {} cut value based clusters and names...".format(cut_value))
    dataframe = build_cut_mapped_names(dataframe, clusterer, cut_value)

    outputfile = inputfile[5: -5]+'-result.csv'
    print("Saving results in {}".format(outputfile))
    dataframe[['Raw name', 'Mapped name', 'cluster',
        'prob', 'Mapped name cut', 'cluster cut']].to_csv(outputfile, index=False)


def clusterize(dataframe, distances):
    """Uses HDBSCAN to cluster the data based on pairwise distance matrix.
    Creates 'cluster' and 'prob' in dataframe.

    Returns:
        clusterer
    """
    # leaf selection methods creates more compact little clusters.
    clusterer = hdbscan.HDBSCAN(
        min_cluster_size=2, metric="precomputed", cluster_selection_method='leaf')

    cluster_labels = clusterer.fit_predict(distances)
    dataframe['cluster'] = cluster_labels
    dataframe['prob'] = clusterer.probabilities_
    return clusterer


def build_mapped_names(dataframe):
    """Build 'Mapped name" column.
    Mapped name is more frequent Clean name or Clean name if not clustered.
    """
    # Mapped name is more frequent clean name.
    dataframe['Mapped name'] = dataframe.groupby('cluster')['Clean name'].transform(
        lambda x: x.value_counts().index[0]).to_frame()
    # Unclustered names will keep their clean names.
    dataframe['Mapped name'] = np.where(
        dataframe.cluster == -1, dataframe['Clean name'], dataframe['Mapped name'])
    return dataframe


def build_cut_mapped_names(dataframe, clusterer, cut_value):
    """Build 'Mapped name cut' and 'cluster cut' columns.
    Mapped name is more frequent Clean name or Clean name if not clustered.
    """
    cluster_labels_cut = clusterer.single_linkage_tree_.get_clusters(
        cut_value, min_cluster_size=2)
    dataframe['cluster cut'] = cluster_labels_cut
    dataframe['Mapped name cut'] = dataframe.groupby('cluster cut')['Clean name'].transform(
        lambda x: x.value_counts().index[0]).to_frame()
    dataframe['Mapped name cut'] = np.where(
        dataframe.cluster == -1, dataframe['Clean name'], dataframe['Mapped name'])
    return dataframe
